import java.io.*;
import java.net.*;
import java.util.Scanner; 
public class A2 {
	public static void main(String[] args) throws IOException {
		
		Scanner kb = new Scanner (System.in);
		System.out.print("please input the address: ");
		String input = kb.nextLine();
		System.out.println("please wait...");

		URL address = new URL(input); 
		Scanner in = new Scanner(address.openStream());
		
		
		while (in.hasNext()) {		
			String next = in.next();
			if (next.contains("href=\"http://") || 
				next.contains("href=\"https://")) {
				System.out.println("\t"+next.substring(next.indexOf("\"")+1,next.lastIndexOf("\"")));
			}
			if (next.contains("<title>")) {
				System.out.print(next.substring(next.indexOf(">")+1,next.length()));
				while (!next.contains("</title>")) {
					System.out.print(" "); 
					next = in.next(); 
					if (next.toString().contains("</title>")) {
						System.out.print(next.substring(0,next.length()-8));
					}
					else { 
						System.out.print(next.substring(0,next.length()));
					}
				}
				System.out.println();
			}
		}
	}
}

